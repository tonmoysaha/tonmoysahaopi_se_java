package com.sweetitech.repository;

import org.springframework.data.repository.CrudRepository;

import com.sweetitech.model.security.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {

	

}
