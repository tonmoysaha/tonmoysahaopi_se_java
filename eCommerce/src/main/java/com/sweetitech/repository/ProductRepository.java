package com.sweetitech.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.sweetitech.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {
    
	List<Product> findTop5ByOrderByProfitPercentageDesc();
	//SELECT p FROM  Product p WHERE ( p.profitPercentage IN (SELECT TOP (5) pp.profitPercentage FROM Product pp))
     
	

}
