package com.sweetitech.repository;

import org.springframework.data.repository.CrudRepository;

import com.sweetitech.model.Admin;

public interface AdminRepository extends CrudRepository<Admin, Long> {

	Admin findByUsername(String username);



}
