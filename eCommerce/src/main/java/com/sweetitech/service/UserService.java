package com.sweetitech.service;

import java.util.Set;

import com.sweetitech.model.Admin;
import com.sweetitech.model.security.Role;
import com.sweetitech.model.security.UserRoles;

public interface UserService {

	Admin createAdmin(Admin admin, Set<UserRoles> userRoles);
	Admin save(Admin admin);

}
